using System;

namespace Arnible.MathModeling.Analysis.Optimization
{
  public class NotAbleToOptimizeException : Exception
  {
    // intentionally empty
  }
}