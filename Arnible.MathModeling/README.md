# Arnible.MathModeling

Math modeling library is split into the following areas:
* [Number](./Number.cs) value type and supporting it LINQ operations.
* [Algebra](./Algebra) namespace with of various mathematical structures like group, rings and derived from them operators.
* [Analysis](./Analysis) namespace with derivative value types and based on it optimization algorithms.
* [Geometry](./Geometry) namespace with definition of cartesian and hyperspherical coordinate system.

`Arnible.MathModeling` is numeric math modeling library. 
For math modeling focused on symbols, rather then numbers, see [Arnible.MathModeling.Formal](./../Arnible.MathModeling.Formal).
